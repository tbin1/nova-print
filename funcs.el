(defun nova-print/export-princexml ()
  "Export to html and post process with princexml"
  (interactive)
  (let* ((current-dir (shell-quote-argument (file-name-directory buffer-file-name)))
         (nova-print-dir "/Users/thomas/.emacs.d/nova-print/print")
         (headers-dir "/Users/thomas/.emacs.d/nova-print/headers")
         (file-name-base (substring (shell-quote-argument buffer-file-name) 0 -4))
         (in (format "%s.html" (file-name-base)))
         (tmp (format "%s-out.pdf" (file-name-base)))
         (out (format "%s.pdf" (file-name-base))))

    (shell-command (format "ln -s %s %s" nova-print-dir current-dir))
    (shell-command (format "ln -s %s %s" headers-dir current-dir))

    (org-html-export-to-html)

    (shell-command (format "prince %s -o %s" in out))
    (dotimes (i 2)
      (shell-command
       (format "pdftk %s cat 2-end output %s && mv %s %s"
               out tmp tmp out)))
    (shell-command "mkdir -p exports")
    (shell-command (format "mv %s ./exports" out))
    (shell-command (format "rm %s && rm %s" (concat current-dir "headers") (concat current-dir "print")))))
