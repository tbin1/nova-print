# 1 "contract.mligo"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4











































# 32 "<command-line>" 2
# 1 "contract.mligo"
type storage = {
  value : nat ;
  age : (nat, nat) map ;
}

type action =
  | Add of nat
  | Multiply of nat

let add (s: storage) (b: nat): storage =
{ value = s.value + b ;
  age = s.age ;
}

let multiply (s: storage) (b: nat): storage =
{ value = s.value * b ;
  age = s.age ;
}

let main(p, s : action * storage) =
  let storage =
    match p with
    | Add n -> add s n
    | Multiply n -> multiply s n
  in (([] : operation list), storage)
