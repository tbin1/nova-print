# 1 "/home/thomas/tmp/bar.mligo"
# 1 "<built-in>"
# 1 "<command-line>"
# 31 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4











































# 32 "<command-line>" 2
# 1 "/home/thomas/tmp/bar.mligo"
type amount_t = nat
type investor_t = address
type owner_t = address
type storage_t = {
  authorized_supply : amount_t ;
  balances : (investor_t , amount_t) map ;
  owner : owner_t ;
}

type ret_t = (operation list) * storage_t

let check_owner (s: storage_t) : unit =
  if sender <> s.owner then
    failwith "Only contract owner can mint."
  else
    ()

let get_total_supply (s: storage_t) : amount_t =
  let aux = fun (prev, balance : amount_t * (investor_t * amount_t)) ->
    prev + balance.1 in
  Map.fold aux s.balances 0n

let check_supply (s: storage_t) : unit =
  if get_total_supply s > s.authorized_supply then
    failwith "AuthorizedSupply too low."
  else
    ()

type mint_t = investor_t * amount_t
let mint (p: mint_t) (s : storage_t) : ret_t =
  let check1 = check_owner s in
  let inv = p.0 in
  let amount_ = p.1 in
  let new_inv_balance = (Map.find inv s.balances) + amount_ in
  let new_balances = Map.update inv (Some new_inv_balance) s.balances in
  let new_storage = {
    owner = s.owner ;
    authorized_supply = s.authorized_supply ;
    balances = new_balances ;
  } in
  let check2 = check_supply s in
  (([] : operation list) , new_storage)

type transfer_t = investor_t * investor_t * amount_t
let transfer (p, s : transfer_t * storage_t) : ret_t =
  let inv_fr = p.0 in
  let inv_fr_balance = Map.find inv_fr s.balances in
  let inv_to = p.1 in
  let inv_to_balance = Map.find inv_to s.balances in
  let amount_ = p.2 in
  let check_fr_balance =
    if inv_fr_balance < amount_ then
      failwith "Inv 1 does not have enough tokens."
    else
      ()
  in
  let new_balances =
    let new_inv_fr_bal = Some (abs (inv_fr_balance - amount_)) in
    let new_inv_to_bal = Some (inv_to_balance + amount_) in
    Map.update inv_fr
               new_inv_fr_bal
               (Map.update inv_to new_inv_to_bal s.balances)
  in
  let new_storage = {
    owner = s.owner ;
    authorized_supply = s.authorized_supply ;
    balances = new_balances ;
  } in
  (([] : operation list) , new_storage)

type action =
| Mint of mint_t
| Transfer of transfer_t

let main (p, s: action * storage_t): ret_t =
  match p with
  | Mint n -> mint n s
  | Transfer n -> transfer (n, s)
