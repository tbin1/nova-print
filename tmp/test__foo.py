from pytest_tezos.TestTypes import Nat, AbstractRecord, Map
from dataclasses import dataclass


@dataclass
class Storage(AbstractRecord):
    value: Nat
    age: Nat

    # why is this needed ? wtf ?
    def __repr__(self):
        return super().__repr__()

init_storage = lambda: Storage(Nat(0), Map({Nat(1): Nat(10), Nat(2): Nat(20)}))

def test_ligo(tezos, ligo):
    assert ligo.bin
    assert ligo.compile('contract.mligo')
    assert ligo.compile(Nat(0))
    originate = ligo.compile('contract.mligo').originate(
        tezos.client,
        init_storage(),
    )
    assert originate.inject


def test_compile_originate_wait(tezos, ligo):
    assert tezos.wait(ligo.compile('contract.mligo').originate(
            tezos.client, init_storage()).inject())


def test_ligo_tezos_integration(ligo, tezos):
    breakpoint()
    tx = ligo.compile('contract.mligo').originate(
        tezos.client,
        init_storage(),
    )

    origination = tx.inject()
    assert origination['hash']

    contract_address = tezos.contract_address(origination)
    assert contract_address

    ci = tezos.client.contract(contract_address)
    assert ci
    assert ci.storage()['value'] == 0

    opg = tezos.wait(ci.add(3))

    # ci = tezos.client.contract(contract_address)
    assert ci.storage()['value'] == 3
