type storage = {
  value : nat ;
  age : (nat, nat) map ;
}

type action =
  | Add of nat
  | Multiply of nat

let add (s: storage) (b: nat): storage =
{ value = s.value + b ;
  age = s.age ;
}

let multiply (s: storage) (b: nat): storage =
{ value = s.value * b ;
  age = s.age ;
}

let main(p, s : action * storage) =
  let storage =
    match p with
    | Add n -> add s n
    | Multiply n -> multiply s n
  in (([] : operation list), storage)
